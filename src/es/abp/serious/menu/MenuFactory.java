/**
 * Copyright (c) 2014 Jorge Torregrosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package es.abp.serious.menu;

import es.abp.serious.menu.commands.CreateCmd;
import es.abp.serious.menu.commands.ExitCmd;
import es.abp.serious.menu.commands.ListCmd;

/**
 * Class wrapping most used menu configurations in the application.
 * 
 * @author Jorge Torregrosa <jtorregrosa@dlsi.ua.es>
 */
public class MenuFactory {

    /**
     * Creates a default admin menu.
     * 
     * @param menuTitle
     * @return admin menu.
     */
    public static Menu createAdminDefaultMenu(String menuTitle) {
        MenuBuilder mb = new MenuBuilder(menuTitle);
        return mb
                .beginGroup("Group 1")
                    .beginGroup("Group 1.1 - Esto es un grupo tan largo, tan largo, tan largo que al final no cabía")
                        .addCommand(new ListCmd("List all resources 1.1"))
                        .addCommand(new CreateCmd("Create new resource 1.1"))
                        .addCommand(new ExitCmd("Go back"))
                    .endGroup()
                    .beginGroup("Group 1.2")
                        .addCommand(new ListCmd("List all resources 1.2"))
                        .addCommand(new CreateCmd("Create new resource 1.2"))
                        .addCommand(new ExitCmd("Go back"))
                    .endGroup()
                    .addCommand(new ExitCmd("Go back"))
                .endGroup()
                .beginGroup("Group 2")
                    .beginGroup("Group 2.1")
                        .addCommand(new ListCmd("List all resources 2.1"))
                        .addCommand(new CreateCmd("Create new resource 2.1"))
                        .addCommand(new ExitCmd("Go back"))
                    .endGroup()
                    .beginGroup("Group 2.2")
                        .addCommand(new ListCmd("List all resources 2.2"))
                        .addCommand(new CreateCmd("Create new resource 2.2"))
                        .addCommand(new ExitCmd("Go back"))
                    .endGroup()
                    .addCommand(new ExitCmd("Go back"))
                .endGroup()
                .beginGroup("Group 3")
                    .beginGroup("Group 3.1")
                        .addCommand(new ListCmd("List all resources 3.1"))
                        .addCommand(new CreateCmd("Create new resource 3.1"))
                        .addCommand(new ExitCmd("Go back"))
                    .endGroup()
                    .beginGroup("Group 3.2")
                        .addCommand(new ListCmd("List all resources 3.2"))
                        .addCommand(new CreateCmd("Create new resource 3.2"))
                        .addCommand(new ExitCmd("Go back"))
                    .endGroup()
                    .addCommand(new ExitCmd("Go back"))
                .endGroup()
                .addCommand(new ExitCmd("Exits"))
                .build();
    }
}
