/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.abp.serious.menu.commands;

/**
 * Abstract Command class. It hides all of the necesary code in child classes.
 * 
 * @author Jorge Torregrosa <jtorregrosa@dlsi.ua.es>
 */
public abstract class AbstractCommand implements MenuCommand {
    private MenuCommand parent;
    private final String description;
    
    /**
     * Abstract Command CTOR.
     * 
     * @param description 
     */
    public AbstractCommand(String description){
        this.description = description;
    }
    
    @Override
    public void interact(MenuCommand parent) {
        this.parent = parent;
        interact();
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void addCommand(MenuCommand command) {
        
    }

    @Override
    public void cancel() {
        parent.cancel();
    }
    
    public abstract void interact();
    
}
