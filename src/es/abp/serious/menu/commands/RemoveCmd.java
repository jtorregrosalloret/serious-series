/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.abp.serious.menu.commands;

import es.abp.serious.entity.Resource;

/**
 * This class wraps remove command logic.
 * @author Jorge Torregrosa <jtorregrosa@dlsi.ua.es>
 */
public class RemoveCmd extends AbstractCommand{

    /**
     * RemoveCmd CTOR.
     * 
     * @param description 
     */
    public RemoveCmd(String description) {
        super(description);
    }

    @Override
    public void interact() {
        System.out.println("Do Action!");
    }
}