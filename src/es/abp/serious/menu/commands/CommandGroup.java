/**
 * Copyright (c) 2014 Jorge Torregrosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package es.abp.serious.menu.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import static org.fusesource.jansi.Ansi.Color.GREEN;
import static org.fusesource.jansi.Ansi.Color.RED;
import static org.fusesource.jansi.Ansi.ansi;

/**
 * Wrapper class containing a collection of commands or other groups.
 * 
 * @author Jorge Torregrosa <jtorregrosa@dlsi.ua.es>
 */
public class CommandGroup implements MenuCommand {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    private static final String MENU_SEPARATOR = "-------------------------------------------------------------";

    protected MenuCommand parent;
    protected String description;
    protected List<MenuCommand> commands;

    private boolean goBack;

    /**
     * Command group CTOR.
     * 
     * @param description
     * @param parent 
     */
    public CommandGroup(String description, MenuCommand parent) {
        this.parent = parent;
        this.description = description;
        this.goBack = false;
        commands = new ArrayList<>();
    }

    /**
     * Returns a description of this group.
     * 
     * @return a description
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * Add a new command or group as child of this group.
     * 
     * @param command to be added
     */
    @Override
    public void addCommand(MenuCommand command) {
        commands.add(command);
    }

    /**
     * Prints a breautiful formatted menu to system.out.
     */
    private void printMenu() {
        StringBuilder b = new StringBuilder();
        int spaces = MENU_SEPARATOR.length() - description.length() - 2;

        char[] spacesStr = new char[spaces < 0 ? 0 : spaces];
        Arrays.fill(spacesStr, ' ');

        
        b.append(ansi().bold()).append(LINE_SEPARATOR)
                .append(MENU_SEPARATOR).append(LINE_SEPARATOR)
                .append("|").append(abbreviate(description, MENU_SEPARATOR.length() -2)).append(String.copyValueOf(spacesStr)).append("|").append(LINE_SEPARATOR)
                .append(MENU_SEPARATOR).append(LINE_SEPARATOR)
                .append(ansi().reset());
        for (int i = 0; i < commands.size(); ++i) {
            b.append("[").append(i).append("] ").append(commands.get(i).getDescription()).append(LINE_SEPARATOR);
        }
        b.append(ansi().bold()).append(MENU_SEPARATOR).append(LINE_SEPARATOR);

        System.out.print(b.toString() + ansi().reset());
    }

    /**
     * This method breaks de inner loop in interact(). Performs an step back in
     * the hierarchy.
     */
    @Override
    public void cancel() {
        goBack = true;
    }

    /**
     * Main logic of the class. It prints the menu, wait for user input and
     * run the correct option.
     * 
     * @param parent command
     */
    @Override
    public void interact(MenuCommand parent) {
        do {
            printMenu();
            System.out.print(ansi().bold().fg(GREEN) + "Insert option: " + ansi().reset());
            
                System.out.flush();
            
            Scanner s = new Scanner(System.in);
            String optionStr = s.nextLine();
            int option;
            try {
                option = Integer.parseInt(optionStr);
                commands.get(option).interact(this);
            } catch (NumberFormatException | IndexOutOfBoundsException e) {
                System.out.println(ansi().bold().fg(RED) + "Unknown option '" + optionStr + "'" + ansi().reset());
            }
        } while (!goBack);
    }

    /**
     * Abbreviate a string.
     * 
     * @param str
     * @param maxWidth
     * @return 
     */
    public String abbreviate(final String str, final int maxWidth) {
        return abbreviate(str, 0, maxWidth);
    }

    /**
     * Abbreviate a string.
     * 
     * @param str
     * @param offset
     * @param maxWidth
     * @return 
     */
    public String abbreviate(final String str, int offset, final int maxWidth) {
        if (str == null) {
            return null;
        }
        if (maxWidth < 4) {
            throw new IllegalArgumentException("Minimum abbreviation width is 4");
        }
        if (str.length() <= maxWidth) {
            return str;
        }
        if (offset > str.length()) {
            offset = str.length();
        }
        if (str.length() - offset < maxWidth - 3) {
            offset = str.length() - (maxWidth - 3);
        }
        final String abrevMarker = "...";
        if (offset <= 4) {
            return str.substring(0, maxWidth - 3) + abrevMarker;
        }
        if (maxWidth < 7) {
            throw new IllegalArgumentException("Minimum abbreviation width with offset is 7");
        }
        if (offset + maxWidth - 3 < str.length()) {
            return abrevMarker + abbreviate(str.substring(offset), maxWidth - 3);
        }
        return abrevMarker + str.substring(str.length() - (maxWidth - 3));
    }
}
