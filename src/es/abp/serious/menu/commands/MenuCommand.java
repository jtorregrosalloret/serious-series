/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.abp.serious.menu.commands;

/**
 *
 * @author Jorge Torregrosa <jtorregrosa@dlsi.ua.es>
 */
public interface MenuCommand {

    public void interact(MenuCommand parent);
    
    public String getDescription();
    public void addCommand(MenuCommand command);
    public void cancel();
}
