/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.abp.serious.menu.commands;

/**
 * This class wrap search command logic.
 * @author Jorge Torregrosa <jtorregrosa@dlsi.ua.es>
 */
public class SearchCmd extends AbstractCommand{

    /**
     * SearchCmd CTOR.
     * 
     * @param description 
     */
    public SearchCmd(String description) {
        super(description);
    }

    @Override
    public void interact() {
        System.out.println("Do Action!");
    }
}