/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.abp.serious.menu.commands;

import es.abp.serious.entity.Resource;

/**
 * This class wraps modify command logic.
 * @author Jorge Torregrosa <jtorregrosa@dlsi.ua.es>
 */
public class ModifyCmd extends AbstractCommand {

    /**
     * ModifyCmd CTOR.
     * 
     * @param description 
     */
    public ModifyCmd(String description) {
        super(description);
    }

    @Override
    public void interact() {
        System.out.println("Do Action!");
    }
}
