/**
 * Copyright (c) 2014 Jorge Torregrosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package es.abp.serious.menu;

import es.abp.serious.menu.commands.CommandGroup;
import es.abp.serious.menu.commands.MenuCommand;
import java.util.Stack;

/**
 * Builder class. It constructs a customized menu with user defined commands 
 * adn groups.
 * 
 * @author Jorge Torregrosa <jtorregrosa@dlsi.ua.es>
 */
public class MenuBuilder {
    
    private CommandGroup root;
    private final Stack<CommandGroup> groupStack;
    private String rootMenuTitle;
    
    /**
     * MenuBuilder CTOR.
     * 
     * @param menuTitle 
     */
    public MenuBuilder(String menuTitle){
        rootMenuTitle = menuTitle;
        root = new CommandGroup(rootMenuTitle, null);
        groupStack = new Stack<>();
        groupStack.push(root);
    }
    
    /**
     * Opens a group. Any additions after this call will be appended to a 
     * lower level in the hierarchy.
     * 
     * @param description Command description
     * @return MenuBuilder
     */
    public MenuBuilder beginGroup(String description){
        CommandGroup g = new CommandGroup(description, groupStack.peek());
        groupStack.peek().addCommand(g);
        groupStack.push(g);
        
        return this;
    }
    
    /**
     * Closes a group. Any additions after this call will be appended to a 
     * higher level in the hierarchy.
     * 
     * @return MenuBuilder
     */
    public MenuBuilder endGroup(){
        groupStack.pop();
        
        return this;
    }
    
    /**
     * Adds a new command to the opened group.
     * 
     * @param command
     * @return MenuBuilder
     */
    public MenuBuilder addCommand(MenuCommand command){
        groupStack.peek().addCommand(command);
        
        return this;
    }
    
    /**
     * Constructs the menu.
     * 
     * @return builded menu
     */
    public Menu build(){
        Menu m = new SimpleMenu(root);
        
        root = new CommandGroup(rootMenuTitle, null);
        groupStack.clear();
        groupStack.push(root);
        
        return m;
    }
}
